include("../src/invsyst.jl")
include("../data/bench.jl")
function test(i::Int64)
    F, X, p, p0, eps0 = bench(i)

    D1, B1, h1, Nu1 = invsyst(F, X, p)
    
    D0, B, h, Nu = invsyst(F, X, p0, eps0)

    J0 = Jacobian(F,D0,B,h,Nu, p0)
    R0 = norm(residual(F,D0,B,h,Nu, p0))
    
    
    Xi, D, N = newton(F,D0,B,h,Nu,p0,X,1.e-20)

    R = norm(residual(F,D,B,h,Nu,Xi))
    println(i, " :residual: & \$", R0,"\$ & \$", R, "\$ & ",N," ")
    return Xi, D, N
end


function test(i::Int64, N::Int64)
    F, X, p, p0, eps0 = bench(i)

    D1, B1, h1, Nu1 = invsyst(F, X, p)
    
    D0, B, h, Nu = invsyst(F, X, p0, eps0)

    J0 = Jacobian(F,D0,B,h,Nu, p0)
    R0 = norm(residual(F,D0,B,h,Nu, p0))
    
    
    Xi, D, N = newton(F,D0,B,h,Nu,p0,X,N)

    R = norm(residual(F,D,B,h,Nu,Xi))
    println(i, " :residual: & \$", R0,"\$ & \$", R, "\$ & ",N," ")
    return Xi, D, N
end
