# Certified singularities

Maple code which provides the following functionalities: 

- Generate benchmark systems
- Compute (approximate) inverse system
- Compute overdetermined and square sub-system
- Apply Newton iteration, apply alpha theory for certification
