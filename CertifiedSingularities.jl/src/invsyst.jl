##
## Computation of the inverse system at an isolated singular point by the integration method
## Newton iteration applied to the system defining the (singular point, multiplicity structure) pair.
##
## Author: Bernard Mourrain (bernard.mourrain@inria.fr)
##
## Date: 15/12/2020
##
## Version: 0.0.1
##

using LinearAlgebra, DynamicPolynomials, MultivariateSeries

import Base: (&), (|)

#----------------------------------------------------------------------
function gradient(f,X)
    [differentiate(f,x) for x in X]
end
#----------------------------------------------------------------------
function row_echelon_with_pivots!(A::Matrix{T}, ɛ = T <: Union{Rational,Integer} ? 0 : 100*eps(norm(A,Inf))) where T
    nr, nc = size(A)
    pivots = Vector{Int64}()
    i = j = 1
    while i <= nr && j <= nc
        (m, mi) = findmax(abs.(A[i:nr,j]))
        mi = mi+i - 1
        if m <= ɛ
            if ɛ > 0
                A[i:nr,j] .= zero(T)
            end
            j += 1
        else
            for k=j:nc
                A[i, k], A[mi, k] = A[mi, k], A[i, k]
            end
            d = A[i,j]
            for k = j:nc
                A[i,k] /= d
            end
            for k = 1:nr
                if k != i
                    d = A[k,j]
                    for l = j:nc
                        A[k,l] -= d*A[i,l]
                    end
                end
            end
            append!(pivots,j)
            i += 1
            j += 1
        end
    end
    return A, pivots
end


function matrixof(L::Vector{MultivariateSeries.Series{T,Mon}}, M::AbstractVector) where {T, Mon}
    Idx = MonIndex()
    for (m,i) in zip(M,1:length(M)) Idx[m]=i end

    A = fill(zero(T), length(L), length(Idx))
    for i in 1:length(L)
        for (m,c) in L[i]
            A[i,Idx[m]] = c
        end
    end
    A
end

#----------------------------------------------------------------------
function invsyst!(F::Vector,
                  B0::Vector,
                  IB::Vector,
                  D0::Vector{MultivariateSeries.Series{C,M}},
                  ID::Vector{MultivariateSeries.Series{C,M}},
                  Nu::Vector{Vector{Int64}},
                  X,
                  Xi,
                  theta=1.e-6) where {C,M}
    
    n = length(X)

    MIdx  = Dict{Vector{Int64},Int64}()
    BpIdx = Dict{Monomial,Int64}()
    BvIdx = Dict{Vector{Int64},Int64}()

    J = Int64[]
    N = 0

    for j in 1:length(D0)
        for k in 1:n
            m = B0[j]*X[n+1-k]
            if !in(m,B0)
                N+=1;
                MIdx[[j,n+1-k]] = N
                push!(J,n*(j-1)+k)

                if get(BpIdx,m,0) == 0
                    BpIdx[m]=1
                else
                    BvIdx[[n+1-k,j]] = 1
                end
            end
        end
    end
    #println(BvIdx)
    
    A = fill(zero(Xi[1]), 0, N)

    # commutation relations
    for k in 1:n
        for l in 1:n
            if l != k
                for m in B0 
                    a = fill(zero(C),N)
                    for i in 1:length(D0)
                        if (p = get(MIdx,[i,l],0)) !=0
                            a[p] = dot(X[k]*D0[i],m)
                        end
                        if (p = get(MIdx,[i,k],0)) !=0
                            a[p] = - dot(X[l]*D0[i],m)
                        end
                    end
                    if a != fill(zero(C),N)
                        A = vcat(A, a')
                    end
                end
            end
        end
    end

    # # border monomial relations 
    # for p in MonRel
    #     v = fill(zero(C),N)
    #     v[p[1]]=1
    #     v[p[2]]=-1
    #     #A = vcat(A, v')
    # end
    
    # orthogonality to B0
    for m in B0
        a = [dot(ID[i], m) for i in J]
        if a != fill(zero(C),N)
            A = vcat(A, a')
        end
    end

    # A, piv = row_echelon_with_pivots!(A)
    # r = length(piv)
    # println("\nCommutation:\n",A, "\npiv: ", piv)
    
    # orthogonality to F
    for f in F
        a = [(ID[j]|f)(Xi,X) for j in J]
        if a != fill(zero(C),N)
            A = vcat(A, a')
        end
    end

    print("[",size(A,1), "x", size(A,2), "]  ")
    if size(A,1) != 0
        A, piv = row_echelon_with_pivots!(A,theta)
        r = length(piv)
    end
    
    # if A is null
    if size(A,1) == 0 || r==0
        n = length(IB)
        h = 0 
        for i in 1:N
            m = IB[i]
            push!(B0,m)
            lambda = dual(m*one(C))
            push!(D0,lambda)
            h += 1
            j = length(D0)
            for k in 1:n
                m = B0[j]*X[n+1-k]
                Ilambda = integrate(lambda,X,n+1-k)
                if !in(m,B0)
                    push!(ID, Ilambda)
                    push!(IB, m)
                    #Idx[[j,n+1-k]] = length(IB)
                end
            end
        end
        return h
    end
    
    #K  = LinearAlgebra.nullspace(A)
    #D1 = K'*L

    #print(eps(norm(A,Inf)), " ")
    #println("\nA: ",A, "\n",piv)
    A = inv(A[1:r,piv])*A[1:r,:]
    #println("\nA: ",A)

    npiv = filter(i->!in(i,piv), collect(1:N))
    #println("\nPivots: ", piv, " ", r, " " , npiv)
    #println("nu: ", length(D0), " ", J[piv], " ", J[npiv])

    #println("Idx: ", BvIdx)
    # Basis of the null space of A
    for i in npiv
        lambda = ID[J[i]]- sum(A[j,i]*ID[J[piv[j]]] for j in 1:r)
        push!(D0, lambda)
        push!(B0, IB[J[i]])
        j = length(D0)
        for l in J[piv]
            u = n-(l-1)%n
            v = div(l-1,n)+1
            m = X[u]*B0[v]
            push!(Nu,[j,u,v, get(BvIdx, [u,v], 0)])
        end

        for k in 1:n
            m = B0[j]*X[n+1-k]
            Ilambda = integrate(lambda,X,n+1-k)
            #println(">> ", lambda, " ", X[n+1-k], "-> ", Ilambda, " ", m)
            if !in(m,B0)
                push!(ID, Ilambda)
                push!(IB, m)
            end
        end
    end
    length(npiv)
 end

function invsyst(F::Vector, X = variables(F), Xi= fill(0,length(X)), theta=1.e-6)
    B = [one(X[1])]
    D = [dual(one(F[1]*one(Xi[1])))]
    ID = typeof(D[1])[]
    IB = []
    Nu = Vector{Int64}[] 
    H =  Int64[]
    n = length(X)
    for k in 1:n
        m = B[1]*X[n+1-k]
        Ilambda = integrate(D[1],X,n+1-k)
        push!(ID, Ilambda)
        push!(IB, m)
    end
    mu = 0;
    h  = 1
    while h != 0
        print(h," ")
        push!(H,h)
        mu += h
        h = invsyst!(F,B,IB,D,ID,Nu,X,Xi,theta)
    end
    println(": ",mu)

    D, B, H, Nu
end

function diff(F,D)
    R = typeof(F[1])[]
    for d in D
        for f in F
            push!(R, d|f)
        end
    end
    R
end

function Jacobian(F, D, B, h, Nu, Xi = fill(0.0,length(variables(B))), X = variables(F))
    e = length(F)
    n = length(X)
    r = length(B)
    p = length(Nu)
    o = length(h)

    cv = div(n*(n-1),2)
    
    nC = 0 
    if o > 2
        nC = sum(h[i]*sum(h[j] for j in 1:i-2) for i in 3:o)*cv
    end
    #println("\nJ: " ,nC," ",e*r," ",n+p)
    JF = fill(zero(Xi[1]),nC+e*r,n+p)

    # differential of D w.r.t. Nu = [nu_{k,i,j}]
    dD = fill(zero(D[1]),r,p)
    for t in 1:p
        l = Nu[t][1]
        u = Nu[t][2]
        v = Nu[t][3]
        for k in 2:r
            if l==k
                dD[k,t] = integrate(D[v],X,u)
            elseif l< k
                dD[k,t] = sum(D[k][ X[i]*B[j] ]*integrate(dD[j,t],X,i) for i in 1:n for j in 1:(k-1))
            end
        end
    end

    # differential of commutation
    if nC > 0
        c = 0
        for k in 1:r
            d = maxdegree(D[k])
            if d >= 2
                s = sum(h[i] for i in 1:d-1)
                for i1 in 1:n
                    for i2 in i1+1:n
                        for t in 1:p
                            l = Nu[t][1]
                            u = Nu[t][2]
                            v = Nu[t][3]
                            dC = zero(D[1])
                            if k==l
                                if u == i1
                                    dC = X[i2]*D[v]
                                elseif u == i2
                                    dC = -X[i1]*D[v]
                                end
                            elseif l < k
                                dC = sum( D[k][ X[i1]*B[j] ]* (X[i2]*dD[j,t]) for j in 1:(k-1) ) -
                                    sum( D[k][ X[i2]*B[j] ]* (X[i1]*dD[j,t]) for j in 1:(k-1) )
                            end
                            for m in 1:s
                                JF[c+m,n+t] = dC[ B[m] ]
                            end
                        end
                        #println("d: ", d, " s: ", s, " c: ", c, "    i,i': ", i1, " ", i2)
                        c+=s
                    end
                end
                
            end
        end
    end

    # differential of orthonality to F
    dF = [gradient(f,X) for f in F]
    for k in 1:r
        for m in 1:e
            for i in 1:n
                JF[nC+e*(k-1)+m,i] = (D[k]|dF[m][i])(Xi,X)
            end

            for i in 1:p
                JF[nC+e*(k-1)+m,n+i] = (dD[k,i]|F[m])(Xi,X)
            end
        end
    end
    #println(">>\n",JF)
    JF
end

function residual(F,D,B,h,Nu,Xi= fill(0.0,length(variables(B))), X = variables(F))
    e = length(F)
    n = length(X)
    r = length(B)
    p = length(Nu)
    o = length(h)
    cv = div(n*(n-1),2)
    
    nC = 0 
    if o > 2
        nC = sum(h[i]*sum(h[j] for j in 1:i-2) for i in 3:o)*cv
    end
    

    XD = fill(zero(D[1]),r,n)
    for k in 2:r
        for i in 1:n
            XD[k,i] = sum(D[k][X[i]*B[j]] * D[j] for j in 1:(k-1))
        end
    end

    R = fill(zero(Xi[1]),nC+e*r)
    
    # commutation
    if nC > 0
        c = 0
        for k in 1:r
            d = maxdegree(D[k])
            if d >= 2
                s = sum(h[i] for i in 1:d-1)
                for i1 in 1:n
                    for i2 in i1+1:n
                        C = sum( D[k][X[i1]*B[j]] * XD[j,i2] for j in 1:(k-1) ) -
                            sum( D[k][X[i2]*B[j]] * XD[j,i1] for j in 1:(k-1) )
                        for m in 1:s
                            R[c+m] = C[B[m]]
                        end
                        c+=s
                    end
                end

            end
        end
    end

    # orthonality to F
    dF = [gradient(f,X) for f in F]
    for k in 1:r
        for m in 1:e
            for i in 1:n
                R[nC+e*(k-1)+m] = (D[k]|F[m])(Xi,X)
            end
        end
    end
    R
end

function newD!(D, B, Nu, dNu, X = variables(B))
    n = length(X)
    r = length(B)
    p = length(Nu)

    for p in 1:length(Nu)
        nu = Nu[p]
        k = nu[1]
        i = nu[2]
        j = nu[3]
        if nu[4] == 0 
            D[k][X[i]*B[j]] += dNu[p]
        end
    end
    
    ID = fill(zero(D[1]),r,n)

    for i in 1:n
        ID[1,i] = integrate(D[1],X,i)
    end
    
    for k in 2:r
        D[k] = sum( D[k][X[i]*B[j]]*ID[j,i] for i in 1:n for j in 1:(k-1))  
        for i in 1:n
            ID[k,i] = integrate(D[k],X,i)
        end
    end
    D
end

function iter_newton!(F,D,B,h,Nu,Xi,X)
    n = length(X)
    R = residual(F,D,B,h,Nu,Xi,X)
    print( " r: ", norm(R), " ")
        J = Jacobian(F,D,B,h,Nu,Xi,X)
    #print(" J",size(J,1),"x",size(J,2)," ") #,svd(J).S," " )
    dV = -J\R
    beta = norm(dV)
    Xi = Xi + dV[1:n]
    dNu = dV[n+1:end]

    D = newD!(D,B,Nu,dNu,X)
    Xi, D, beta
end

function newton(F,D,B,h,Nu,Xi,X,N::Int64=3)

    for i in 1:N
        Xi, D, beta =  iter_newton!(F,D,B,h,Nu,Xi,X)
        println()
    end
    Xi, D, N
end

function newton(F,D,B,h,Nu,Xi,X, err::Float64, Nmax=10)
    e = norm(residual(F,D,B,h,Nu,Xi,X))
    beta0 = 100; beta=1;
    i=0
    while i<=3 || (beta0/beta > 10 && i < Nmax && e > err) 
        print(i, ":: ")
        beta0 = beta
        Xi, D, beta =  iter_newton!(F,D,B,h,Nu,Xi,X)
        e = norm(residual(F,D,B,h,Nu,Xi,X))
        print(" beta1: ",beta, " beta0:", beta0, " ")
        i+=1
        println()
    end
    Xi, D, i-1
end
