include("../src/invsyst.jl")
include("../src/data.jl")
include("test.jl")

F, X, p, p0, eps0 = bench(1)

D1, B1, h1, Nu1 = invsyst(F, X, p)

D0, B, h, Nu = invsyst(F, X, p0, eps0)

J0 = Jacobian(F,D0,B,h,Nu, p0)
R0 = residual(F,D0,B,h,Nu, p0)
norm(R0)

Xi, D = newton(F,D0,B,h,Nu,p0,X,1.e-32)



