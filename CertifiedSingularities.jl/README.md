# Certified singularities

Julia codes which provides the following functionalities: 

- Compute the approximate inverse system nearby a point 
- Newton iteration  to improve quadratically the point and its inverse system.