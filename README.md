# Certified Singularities

A certified iterative method for isolated singular roots.

Angelos Mantzaflaris, Bernard Mourrain, Agnes Szanto
