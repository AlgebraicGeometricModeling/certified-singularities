include("../src/invsyst.jl")


X = @polyvar x1 x2

#F = [x1^2+0.0, x2^2+0.0]
#F = [x2+0.0, x1^2+0.0]
F = [x1 - x2 + x1^2*1.0, x1 - x2 + x2^2*1.0]
#F = [3.0*x2^2+6*(x1+x2)^5,6*x1^5+6*(x1+x2)^5]
#a=29/32; F = [2*a*x1^3-2*x1*x2, x2-x1^2]
Xi0 = [-0.001, 0.001]


# X = @polyvar x1 x2 x3
# F = [x1^3+x2^2+x3^2-1.0, x1^3+x2^3+x3^2-1.0, x1^2+x2^2+x3^3-1.0]
# Xi0 = [-0.01, 1.01, 0.01]


D0, B, h, Nu = invsyst(F,X,Xi0,1.e-2)
#D, B, h, Nu = invsyst(F)

println(Xi0, " ", D0)    
println(norm(Delta(F,D0,B,h,Nu,Xi0,X)))

Xi, D = newton(F,D0,B,h,Nu,Xi0,X,5)



